.\Orchastrator.ps1 -imageResourceGroup  "ImageBakeryTemp2016" `
	-location  "North Europe" `
	-SubID  "3664f4c8-49c3-438b-ba1f-31176f8ede2e" `
	-TenantID  "bf0465f4-f8c0-4ff4-978d-af5315afa795" `
	-SoftwareFileName  "Server2016Software" `
	-installScriptURI  "https://bitbucket.org/ma1co1m/sigtesting/raw/39a0ab59fc12360a1b9c7e3bcde01051f3e237e2/Install-AppsServer.ps1" `
	-imageName  "CustomImg2016" `
	-imageTemplateName  "imageTemplateMultiApps" `
	-runOutputName  "Server2016Client" `
	-vmSize  "Standard_D2_v2" `
	-Extpublisher  "MicrosoftWindowsServer" `
	-Extoffer  "WindowsServer" `
	-Extsku  "2016-Datacenter" `
	-ImageDefinitionName  "Server2016" `
	-Intpublisher  "Malc" `
	-Intoffer  "WindowsServer" `
	-Intsku  "2016-Datacenter" `
	-EndOfLifeDate  "2021-12-31" `
	-ImageVersionName  "1.0.0" 	`
	#-LoadModules