# Software install Script
#
# Applications to install:
#
# EMET
# 
# Windows feature to add
# 
# Net-Framework-Core, Net-Framework-45-Features, Powershell-ISE, XPS-Viewer


#region Set logging 
$logFile = "c:\temp\" + (get-date -format 'yyyyMMdd') + '_softwareinstall.log'
function Write-Log {
    Param($message)
    Write-Output "$(get-date -format 'yyyyMMdd HH:mm:ss') $message" | Out-File -Encoding utf8 $logFile -Append
}
#endregion

#region Add basic windows features
try {
    Install-WindowsFeature -Name "Net-Framework-45-Features"
    Install-WindowsFeature -Name "Powershell-ISE"
    Install-WindowsFeature -Name "XPS-Viewer"
}
catch {
    $ErrorMessage = $_.Exception.message
    write-log "Error installing Windows Feature: $ErrorMessage"
}
#endregion

#region Add advanced windows features
try {
    Install-WindowsFeature -Name "Net-Framework-Core" -Source "C:\Temp\sxs"
    }
catch {
    $ErrorMessage = $_.Exception.message
    write-log "Error installing Windows Feature: $ErrorMessage"
}
#endregion

#region Emet
try {
    Start-Process -filepath msiexec.exe -Wait -ErrorAction Stop -ArgumentList '/i', 'c:\temp\EMET.msi', '/quiet'
    # if (Test-Path "C:\Program Files (x86)\Foxit Software\Foxit Reader\FoxitReader.exe") {
    #     Write-Log "Foxit Reader has been installed"
    # }
    # else {
    #     write-log "Error locating the Foxit Reader executable"
    # }
catch {
    $ErrorMessage = $_.Exception.message
    write-log "Error installing EMET: $ErrorMessage"
}
#endregion

#region Sysprep Fix
# Fix for first login delays due to Windows Module Installer
try {
    ((Get-Content -path C:\DeprovisioningScript.ps1 -Raw) -replace 'Sysprep.exe /oobe /generalize /quiet /quit', 'Sysprep.exe /oobe /generalize /quit /mode:vm' ) | Set-Content -Path C:\DeprovisioningScript.ps1
    write-log "Sysprep Mode:VM fix applied"
}
catch {
    $ErrorMessage = $_.Exception.message
    write-log "Error updating script: $ErrorMessage"
}
#endregion

#region Time Zone Redirection
$Name = "fEnableTimeZoneRedirection"
$value = "1"
# Add Registry value
try {
    New-ItemProperty -ErrorAction Stop -Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows NT\Terminal Services" -Name $name -Value $value -PropertyType DWORD -Force
    if ((Get-ItemProperty "HKLM:\SOFTWARE\Policies\Microsoft\Windows NT\Terminal Services").PSObject.Properties.Name -contains $name) {
        Write-log "Added time zone redirection registry key"
    }
    else {
        write-log "Error locating the Teams registry key"
    }
}
catch {
    $ErrorMessage = $_.Exception.message
    write-log "Error adding teams registry KEY: $ErrorMessage"
}
#endregion