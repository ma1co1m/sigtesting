.\Orchastrator.ps1 -imageResourceGroup  "ImageBakeryTemp20H2-evd" `
	-location  "North Europe" `
	-SubID  "3664f4c8-49c3-438b-ba1f-31176f8ede2e" `
	-TenantID  "bf0465f4-f8c0-4ff4-978d-af5315afa795" `
	-SoftwareFileName  "Windows10Software" `
	-installScriptURI  "https://bitbucket.org/ma1co1m/sigtesting/raw/820db3b5915de0a010ae9935de750d5a506036e7/Install-Applications.ps1" `
	-imageName  "CustomImg20h2" `
	-imageTemplateName  "imageTemplateMultiApps" `
	-runOutputName  "Windows10Client" `
	-vmSize  "Standard_B8ms" `
	-Extpublisher  "MicrosoftWindowsDesktop" `
	-Extoffer  "Windows-10" `
	-Extsku  "20h2-evd" `
	-ImageDefinitionName  "Windows10-20h2-evd" `
	-Intpublisher  "Malc" `
	-Intoffer  "Windows-10" `
	-Intsku  "20h2-evd" `
	-EndOfLifeDate  "2021-12-31" `
	-ImageVersionName  "1.0.0" `
	#-LoadModules