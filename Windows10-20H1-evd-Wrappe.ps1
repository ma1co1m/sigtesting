.\Orchastrator.ps1 -imageResourceGroup  "ImageBakeryTemp20H1-evd" `
	-location  "North Europe" `
	-SubID  "3664f4c8-49c3-438b-ba1f-31176f8ede2e" `
	-TenantID  "bf0465f4-f8c0-4ff4-978d-af5315afa795" `
	-SoftwareFileName  "Windows10Software" `
	-installScriptURI  "https://bitbucket.org/ma1co1m/sigtesting/raw/52377b1d459e94993ebcaa0a10e803eaec844c3d/Install-Applications.ps1" `
	-imageName  "CustomImg20h1" `
	-imageTemplateName  "imageTemplateMultiApps" `
	-runOutputName  "Windows10Client" `
	-vmSize  "Standard_B8ms" `
	-Extpublisher  "MicrosoftWindowsDesktop" `
	-Extoffer  "Windows-10" `
	-Extsku  "20h1-evd" `
	-ImageDefinitionName  "Windows10-20h1-evd" `
	-Intpublisher  "Malc" `
	-Intoffer  "Windows-10" `
	-Intsku  "20h1-evd" `
	-EndOfLifeDate  "2021-12-31" `
	-ImageVersionName  "1.0.0" `
	#-LoadModules