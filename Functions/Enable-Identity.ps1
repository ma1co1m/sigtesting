

Function Enable-Identity{

  [CmdletBinding()]
  
  Param(
    [string]$imageResourceGroup,
    [string]$location
  )

  $Scriptpath = $PSScriptRoot

# Set Variables for the commands
# Destination image resource group name
#$imageResourceGroup = 'ImageBakeryTest2'
# Azure region
# Supported Regions East US, East US 2, West Central US, West US, West US 2, North Europe, West Europe
#$location = 'North Europe'
# Get the subscription ID
$subscriptionID = (Get-AzContext).Subscription.Id

# Get the PowerShell modules
#'Az.ImageBuilder', 'Az.ManagedServiceIdentity' | ForEach-Object {Install-Module -Name $_}

# Start by creating the Resource Group
# the identity will need rights to this group

$RGTest = Get-AzResourceGroup -Name $imageResourceGroup -Location $location -ErrorAction SilentlyContinue

if ($null -eq $RGTest) {
  New-AzResourceGroup -Name $imageResourceGroup -Location $location

  }

# Create the Managed Identity
# Use current time to verify names are unique
[int]$timeInt = $(Get-Date -UFormat '%s')
$imageRoleDefName = "Azure Image Builder Image Def $timeInt"
$identityName = "GalleryIdentity$timeInt"

# Create the User Identity
Write-Host "Ceating identity" -ForegroundColor Green
New-AzUserAssignedIdentity -ResourceGroupName $imageResourceGroup -Name $identityName

# Assign the identity resource and principle ID's to a variable
$identityNamePrincipalId = (Get-AzUserAssignedIdentity -ResourceGroupName $imageResourceGroup -Name $identityName).PrincipalId

# Assign permissions for identity to distribute images
# downloads a .json file with settings, update with subscription settings
#$myRoleImageCreationUrl = 'https://raw.githubusercontent.com/danielsollondon/azvmimagebuilder/master/solutions/12_Creating_AIB_Security_Roles/aibRoleImageCreation.json'
#$myRoleImageCreationPath = ".\myRoleImageCreation.json"
# Download the file
#Invoke-WebRequest -Uri $myRoleImageCreationUrl -OutFile $myRoleImageCreationPath -UseBasicParsing

Write-Host "Create role" -ForegroundColor Green
$myRoleImageCreationPath = (get-item $scriptPath ).parent.FullName.ToString() + "\Templates\RoleImageCreation.json"
$myRoleImageOutputPath = (get-item $scriptPath ).parent.FullName.ToString() + "\RoleImageCreation.json"

# Update the file
$Content = Get-Content -Path $myRoleImageCreationPath -Raw
$Content = $Content -replace '<subscriptionID>', $subscriptionID
$Content = $Content -replace '<rgName>', $imageResourceGroup
$Content = $Content -replace 'Azure Image Builder Service Image Creation Role', $imageRoleDefName
$Content | Out-File -FilePath $myRoleImageOutputPath -Force

# Create the Role Definition
Write-Host "Add role definition" -ForegroundColor Green
New-AzRoleDefinition -InputFile $myRoleImageOutputPath

#TODO need to do something here ;# Defines the values for the resource's Ensure property.
enum Ensure {
  # The resource must be absent.
  Absent
  # The resource must be present.
  Present
}

# [DscResource()] indicates the class is a DSC resource.
[DscResource()]
class NameOfResource {
  # A DSC resource must define at least one key property.
  [DscProperty(Key)]
  [string] $P1
  
  # Mandatory indicates the property is required and DSC will guarantee it is set.
  [DscProperty(Mandatory)]
  [Ensure] $P2
  
  # NotConfigurable properties return additional information about the state of the resource.
  # For example, a Get() method might return the date a resource was last modified.
  # NOTE: These properties are only used by the Get() method and cannot be set in configuration.
  [DscProperty(NotConfigurable)]
  [Nullable[datetime]] $P3
  
  [DscProperty()]
  [ValidateSet("val1", "val2")]
  [string] $P4
  
  # Gets the resource's current state.
  [NameOfResource] Get() {
    # NotConfigurable properties are set in the Get method.
    $this.P3 = something
    # Return this instance or construct a new instance.
    return $this
  }
  
  # Sets the desired state of the resource.
  [void] Set() {
  }
  
  # Tests if the resource is in the desired state.
  [bool] Test() {
     return $true
  }
}

# Grant the Role Definition to the Image Builder Service Principle
$RoleAssignParams = @{
    ObjectId = $identityNamePrincipalId
    RoleDefinitionName = $imageRoleDefName
    Scope = "/subscriptions/$subscriptionID/resourceGroups/$imageResourceGroup"
  }

#TODO add some conditional checking, for now just going to add a sleep
Start-Sleep -Seconds 30

New-AzRoleAssignment @RoleAssignParams

# Verify Role Assignment
Get-AzRoleAssignment -ObjectId $identityNamePrincipalId | Select-Object DisplayName,RoleDefinitionName
}
