Function Add-Software{

    [CmdletBinding()]
    
    Param(
      [string]$imageResourceGroup,
      [string]$zipname
    )
  
    $Scriptpath = $PSScriptRoot

    $location = (Get-AzResourceGroup -Name $imageResourceGroup).Location
    #Create a storage account for the software
    # Use current time to verify names are unique
    [int]$timeInt = $(Get-Date -UFormat '%s')

    $storageAccount = New-AzStorageAccount -ResourceGroupName $imageResourceGroup `
     -AccountName "software$timeInt" `
     -Location $location `
     -SkuName "Standard_LRS"

    #Get te context
    $ctx = $storageAccount.Context

  #Create the blob
    $containerName = "softwareblob"
    New-AzStorageContainer -Name $containerName -Context $ctx -Permission blob

    #Upload the software zip
    # upload a file to the default account (inferred) access tier
    $zipfile = (get-item $scriptPath ).parent.FullName.ToString() + "\Software\$zipname"
    Set-AzStorageBlobContent -File $zipfile `
    -Container $containerName `
    -Blob $zipname `
    -Context $ctx 

    #Create SAS token
    $StartTime = Get-Date
    $EndTime = $startTime.AddHours(2.0)
    $SASuri = New-AzStorageBlobSASToken -Container $containerName -Context $ctx -Blob $zipname -Permission rwd -StartTime $StartTime -ExpiryTime $EndTime -FullUri
    $SASuri
} 