Function Build-Template{

    [CmdletBinding()]
    
    Param(
      [string]$imageResourceGroup,
      [string]$archiveSas,
      [string]$installScriptURI,
      [string]$imageName,
      [string]$imageTemplateName,
      [string]$runOutputName,
      [string]$vmSize,
      [string]$publisher,
      [string]$offer,
      [string]$sku,
      [string]$SoftwareFileName
    )
  
    $Scriptpath = $PSScriptRoot

# location (see possible locations in main docs)
$location = (Get-AzResourceGroup -Name $imageResourceGroup).Location
# your subscription, this will get your current subscription
$subscriptionID = (Get-AzContext).Subscription.Id

# Set the Template File Path
$templateFilePath = ".\Template\$Win10FileName"
# user-assigned managed identity
$identityName = (Get-AzUserAssignedIdentity -ResourceGroupName $imageResourceGroup).Name
# get the user assigned managed identity id
$identityNameResourceId = (Get-AzUserAssignedIdentity -ResourceGroupName $imageResourceGroup -Name $identityName).Id

$sourcetemplateFilePath = (get-item $scriptPath ).parent.FullName.ToString() + "\Templates\MultiAppsTemplate.json"
$templateFilePath = (get-item $scriptPath ).parent.FullName.ToString() + "\MultiAppsTemplate$sku.json"

# Update the file
$Content = Get-Content -Path $sourcetemplateFilePath -Raw
If($SoftwareFileName){
  $customisescriptpath = (get-item $scriptPath ).parent.FullName.ToString() + "\Software\$SoftwareFileName.txt" 
  $customisescript = Get-Content $customisescriptpath -RAW
  $Content = $Content -replace '<customise>',$customisescript
  $Content = $Content -replace '<software.zip>',"$SoftwareFileName.zip"
}
else{
  $Content = $Content -replace '<customise>',""
}
$Content = $Content -replace '<subscriptionID>', $subscriptionID
$Content = $Content -replace '<rgName>', $imageResourceGroup
$Content = $Content -replace '<region>',$location
$Content = $Content -replace '<runOutputName>',$runOutputName
$Content = $Content -replace '<imageName>',$imageName
$Content = $Content -replace '<imgBuilderId>',$identityNameResourceId
$Content = $Content -replace '<Shared Access Signature to archive file>',$archiveSas
$Content = $Content -replace '<URI to PowerShell Script>',$installScriptURI
$Content = $Content -replace '<vmSize>',$vmSize
$Content = $Content -replace '<publisher>',$publisher
$Content = $Content -replace '<offer>',$offer
$Content = $Content -replace '<sku>',$sku
$Content | Out-File -FilePath $templateFilePath -Force

# Run the deployment
New-AzResourceGroupDeployment -ResourceGroupName $imageResourceGroup -TemplateFile $templateFilePath `
-api-version "2019-05-01-preview" -imageTemplateName $imageTemplateName -svclocation $location

# Verify the template
Get-AzImageBuilderTemplate -ImageTemplateName $imageTemplateName -ResourceGroupName $imageResourceGroup |
Select-Object -Property Name, LastRunStatusRunState, LastRunStatusMessage, ProvisioningState, ProvisioningErrorMessage

# Start the Image Build Process
Start-AzImageBuilderTemplate -ResourceGroupName $imageResourceGroup -Name $imageTemplateName

}



