Function Remove-TempFiles{

    [CmdletBinding()]
    
    Param(
      [string]$imageResourceGroup,
      [string]$imageTemplateName
    )    

    # Remove the template deployment
    Remove-AzImageBuilderTemplate -ImageTemplateName $imageTemplateName -ResourceGroupName $imageResourceGroup -Confirm:$false

    # Remove temp Resource group
    Remove-AzResourceGroup -Name $imageResourceGroup -Confirm:$false -Force

    # $Groups = Get-AzResourceGroup | Where-Object {$_.ResourceGroupName -like "*ImageBakery*"}
    # $Groups
    # $Groups | Remove-AzResourceGroup -Confirm:$false -Force
}