Function Add-ImageGallery{

    [CmdletBinding()]
    
    Param(
      [string]$GalleryResourceGroup,
      [string]$ImageDefinitionName,
      [string]$publisher,
      [string]$offer,
      [string]$sku,      
      [string]$ImageResourceGroup,
      [string]$EndOfLifeDate,
      [string]$ImageVersionName
    )
  
    # Get location based on image 
    $Location = (Get-AzResource -ResourceType Microsoft.Compute/images -ResourceGroupName $ImageResourceGroup).Location
    

    #Get gallery name
    $GalleryName = (Get-AzResource -ResourceType Microsoft.Compute/galleries -ResourceGroupName $GalleryResourceGroup).Name
    
    #Get gallery
    $gallery = Get-AzGallery `
    -Name $GalleryName `
    -ResourceGroupName $GalleryResourceGroup

    #Create definition
    $imageDefinition = New-AzGalleryImageDefinition `
    -GalleryName $gallery.Name `
    -ResourceGroupName $gallery.ResourceGroupName `
    -Location $Location `
    -Name $ImageDefinitionName `
    -OsState generalized `
    -OsType Windows `
    -Publisher $publisher `
    -Offer $offer `
    -Sku $sku


    # Get Image name   
    $ImageName = (Get-AzResource -ResourceType Microsoft.Compute/images -ResourceGroupName $ImageResourceGroup).Name 
    

    #Get the managed image
    $managedImage = Get-AzImage `
    -ImageName $ImageName `
    -ResourceGroupName $ImageResourceGroup

    $SourceImageId = $managedImage.Id.ToString()


    $region1 = @{Name='UK South';ReplicaCount=1}
    $region2 = @{Name='North Europe';ReplicaCount=2}
    $targetRegions = @($region1,$region2)
    #$job = $imageVersion = New-AzGalleryImageVersion `
    $imageVersion = New-AzGalleryImageVersion `
    -GalleryImageDefinitionName $ImageDefinitionName `
    -GalleryImageVersionName $ImageVersionName `
    -GalleryName $GalleryName `
    -ResourceGroupName $GalleryResourceGroup `
    -Location $Location `
    -TargetRegion $targetRegions  `
    -SourceImageId $SourceImageId `
    -PublishingProfileEndOfLifeDate $EndOfLifeDate `
    #-asJob
    
    $Currentstate = (Get-AzGalleryImageVersion -ResourceGroupName $GalleryResourceGroup -GalleryName $GalleryName -GalleryImageDefinitionName $ImageDefinitionName).ProvisioningState
    If ($Currentstate -ne "Succeeded")
    {
      Throw "The image failed to upload to the gallery"
    }


}

