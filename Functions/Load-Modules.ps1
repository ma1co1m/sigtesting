Function Load-Modules{
    'Az.ImageBuilder', 'Az.ManagedServiceIdentity', 'Az.Storage' | ForEach-Object {Install-Module -Name $_ -Confirm:$false -Force -AllowClobber}
  }
  