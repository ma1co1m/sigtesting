
[CmdletBinding()]
    
Param(

[parameter(mandatory=$true)] [string]$imageResourceGroup,
[parameter(mandatory=$true)] [string]$location,
[parameter(mandatory=$true)] [string]$SubID,
[parameter(mandatory=$true)] [string]$TenantID,
[parameter(mandatory=$false)] [string]$SoftwareFileName,
[parameter(mandatory=$false)] [string]$installScriptURI,
[parameter(mandatory=$true)] [string]$imageName,
[parameter(mandatory=$true)] [string]$imageTemplateName,
[parameter(mandatory=$true)] [string]$runOutputName,
[parameter(mandatory=$true)] [string]$vmSize,
[parameter(mandatory=$true)] [string]$Extpublisher,
[parameter(mandatory=$true)] [string]$Extoffer,
[parameter(mandatory=$true)] [string]$Extsku,
[parameter(mandatory=$true)] [string]$ImageDefinitionName,
[parameter(mandatory=$true)] [string]$Intpublisher,
[parameter(mandatory=$true)] [string]$Intoffer,
[parameter(mandatory=$true)] [string]$Intsku,
[parameter(mandatory=$true)] [string]$EndOfLifeDate,
[parameter(mandatory=$true)] [string]$ImageVersionName,
[switch]$LoadModules
)



$Scriptpath = $PSScriptRoot
$SPCreds = Import-Clixml $Home\Credentialstore\prod-Automation.clixml

$AppID = $SPCreds.GetNetworkcredential().username
$Apppwd = $SPCreds.GetNetworkcredential().password

az login --service-principal --username $AppID --password $Apppwd --tenant $TenantID 

Get-AzSubscription -SubscriptionId $SubID

#Load AIB functions
$functionspath = (get-item $scriptPath ).FullName.ToString() + "\Functions"
Write-Host "Loading functions" -ForegroundColor Green
$functionspath = ($Scriptpath).ToString() + "\Functions"
$AllFunctions = Get-ChildItem $functionspath -Recurse
ForEach($function in $AllFunctions)
{
    . $function.FullName
}

Write-Host "Functions Loaded" -ForegroundColor Green

If($LoadModules -eq -$true)
{
#Load required modules
Write-Host "Loading modules" -ForegroundColor Green
 Load-Modules
Write-Host "Modules Loaded" -ForegroundColor Green
}

#Run Enable-Identity
# This sets up a temporary resoruce group and managed idenity for AIB to build a generalised image
Enable-Identity -imageResourceGroup $imageResourceGroup -location $location

If($SoftwareFileName){
#Run Add-Software
Write-Host "Uploading software" -ForegroundColor Green
$archiveSas = (Add-Software -imageResourceGroup $imageResourceGroup -zipname "$SoftwareFileName.zip")[2]
Write-Host "Software uploaded" -ForegroundColor Green
}

#Build the template
Write-Host "Building template" -ForegroundColor Green
Build-Template  -imageResourceGroup $imageResourceGroup `
-archiveSas $archiveSas `
-installScriptURI $installScriptURI `
-imageName $imageName `
-imageTemplateName $imageTemplateName `
-runOutputName $runOutputName `
-vmSize $vmSize `
-publisher $Extpublisher `
-offer $Extoffer `
-sku $Extsku `
-SoftwareFileName $SoftwareFileName 

Write-Host "Template built" -ForegroundColor Green

#Needed to loop here as image did not always return data straight away (can take a number of minutes)
$Location = (Get-AzResource -ResourceType Microsoft.Compute/images -ResourceGroupName $ImageResourceGroup).Location
While ([string]::IsNullOrEmpty($Location)){
    $Location = (Get-AzResource -ResourceType Microsoft.Compute/images -ResourceGroupName $ImageResourceGroup).Location
    Write-Host "Image is not resolving yet" -ForegroundColor Yellow
    Start-Sleep -seconds 30
}
Write-Host "Image is now resolved" -ForegroundColor Green

#Add newly captrured image to the Gallery
$CurrentDate = Get-Date
$Month = $CurrentDate.Month | %{(Get-Culture).DateTimeFormat.GetMonthName($_)}
$Year = $CurrentDate.ToUniversalTime().Year
Add-ImageGallery -GalleryResourceGroup "ImageGallery" `
-ImageDefinitionName "$ImageDefinitionName-$Month-$Year" `
-publisher $Intpublisher `
-offer $Intoffer `
-sku $Intsku-$Month-$Year `
-ImageResourceGroup $imageResourceGroup `
-EndOfLifeDate $EndOfLifeDate `
-ImageVersionName $ImageVersionName

#Tidy up
Remove-TempFiles -imageResourceGroup $imageResourceGroup -imageTemplateName $imageTemplateName


# Create a VM to test 
# $Cred = Get-Credential
# $ArtifactId = (Get-AzImageBuilderRunOutput -ImageTemplateName $imageTemplateName -ResourceGroupName "ImageBakeryTemp").ArtifactId
# New-AzVM -ResourceGroupName "ImageBakeryTemp" -Image $ArtifactId -Name myWinVM01 -Credential $Cred -size Standard_D2_v2

# $ArtifactId = "/subscriptions/3664f4c8-49c3-438b-ba1f-31176f8ede2e/resourceGroups/ImageGallery/providers/Microsoft.Compute/galleries/MalcImageGallery/images/Server2016-April2/versions/1.0.0"
# $Cred = Get-Credential

# New-AzVm -ResourceGroupName "HomeLabSouth-resources" `
# -Name "malctest" -Image $ArtifactId `
# -Location "uksouth" -VirtualNetworkName "NetworkSouth" `
# -SubnetName "default" `
# -PublicIpAddressName "MalcTestnic" `
# -Credential $Cred -size Standard_D2_v2 `
# -OpenPorts 3389
#New-AzVM -ResourceGroupName "ImageBakeryTemp" -Image $ArtifactId -Name myWinVM01 -Credential $Cred -size Standard_D2_v2

######## Useful commands to get available offers, skus etc.  ##########

#Get-AzVMImagePublisher -Location $location | where-object {$_.PublisherName -like "*win*"} | ft PublisherName,Location
#$pubName = 'MicrosoftWindowsDesktop'
#Get-AzVMImageOffer -Location $location -PublisherName $pubName | ft Offer,PublisherName,Location
# Set Offer to 'office-365' for images with O365 
# $offerName = 'office-365'
#$offerName = 'Windows-10'
#Get-AzVMImageSku -Location $location -PublisherName $pubName -Offer $offerName | ft Skus,Offer,PublisherName,Location
#$skuName = '20h1-evd'
#Get-AzVMImage -Location $location -PublisherName $pubName -Skus $skuName -Offer $offerName
#$version = '19041.572.2010091946'
#Get-AzVMImage -Location $location -PublisherName $pubName -Offer $offerName -Skus $skuName -Version $version


